<?php
/**
 * @file
 * XML Transformations - Transformations for processing XML data.
 *
 * This file contains operations for converting between XML strings and
 * internal XML node objects.
 *
 * Copyright 2009 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_xml_operation_TfXMLNodeFromXMLString() {
  return array(
    'category' => t('XML'),
    'label' => t('Convert XML text to XML node'),
    'description' => t('Transforms a valid XML string into node objects, enabling navigation of the XML tree.'),
  );
}

class TfXMLNodeFromXMLString extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('xmlString');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'xmlString') {
      switch ($propertyKey) {
        case 'label':
          return t('XML text');

        case 'expectedType':
          return 'php:type:string';

        case 'dataWidgetTypeHints':
          return array('string:multiline' => TRUE);
      }
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('xmlNode');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'xmlNode') {
      switch ($propertyKey) {
        case 'label':
          return t('XML document node');

        case 'expectedType':
          return 'php:class:DOMDocument';
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    try {
      TfDataWrapper::includeWrapper('TfXMLNodeData');
      $xmlWrapper = new TfXMLNodeData($this->input('xmlString')->data());
    }
    catch (Exception $e) {
      $output->setErrorMessage(t('Unable to parse XML string: !message', array(
        '!message' => $e->getMessage(),
      )));
      return;
    }
    $output->set('xmlNode', $xmlWrapper);
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_xml_operation_TfXMLNodeFromXMLFile() {
  return array(
    'category' => t('XML'),
    'label' => t('Load XML file into XML node'),
    'description' => t('Transforms a valid XML file into node objects, enabling navigation of the XML tree.'),
  );
}

class TfXMLNodeFromXMLFile extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('filepath');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'filepath') {
      switch ($propertyKey) {
        case 'label':
          return t('File path');

        case 'description':
          return t('The path in the local file system where the XML file is located.');

        case 'expectedType':
          return 'php:type:string';
      }
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('xmlNode');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'xmlNode') {
      switch ($propertyKey) {
        case 'label':
          return t('XML document node');

        case 'description':
          return t('The XML object representing the document node.');

        case 'expectedType':
          return 'php:class:DOMDocument';
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    $doc = new DOMDocument();
    $path = realpath(trim($this->input('filepath')->data()));

    if ($doc->load($path, LIBXML_NOCDATA | LIBXML_NOENT | LIBXML_NOBLANKS)) {
      $output->set('xmlNode', $doc);
    }
    else {
      $output->setErrorMessage(t('Unable to load XML file "!filename".', array(
        '!filename' => $path,
      )));
    }
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_xml_operation_TfOuterXMLStringFromXMLNode() {
  return array(
    'category' => t('XML'),
    'label' => t('Convert XML node to XML text'),
    'description' => t('Transforms an XML node object into a valid XML string, including the XML header.'),
  );
}

class TfOuterXMLStringFromXMLNode extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('xmlNode');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'xmlNode') {
      switch ($propertyKey) {
        case 'label':
          return t('XML node');

        case 'expectedType':
          return 'php:class:DOMNode';
      }
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('xmlString');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'xmlString') {
      switch ($propertyKey) {
        case 'label':
          return t('XML text');

        case 'expectedType':
          return 'php:type:string';
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    TfDataWrapper::includeWrapper('TfXMLNodeData'); // for TfXMLTools
    $node = $this->input('xmlNode')->data();
    $output->set('xmlString', TfXMLTools::asXMLDocument($node));
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_xml_operation_TfInnerXMLStringFromXMLNode() {
  return array(
    'category' => t('XML'),
    'label' => t('Extract inner XML contents from XML node'),
    'description' => t('Extracts the contents of an XML node object into a simple string. You probably want to use this in order to retrieve the text of an XML element.'),
  );
}

class TfInnerXMLStringFromXMLNode extends TfOperation {
  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('xmlNode');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'xmlNode') {
      switch ($propertyKey) {
        case 'label':
          return t('XML node');

        case 'expectedType':
          return 'php:class:DOMNode';
      }
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('stringContents');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'stringContents') {
      switch ($propertyKey) {
        case 'label':
          return t('String contents');

        case 'expectedType':
          return 'php:type:string';
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    TfDataWrapper::includeWrapper('TfXMLNodeData'); // for TfXMLTools
    $node = $this->input('xmlNode')->data();
    $output->set('stringContents', TfXMLTools::innerXML($node));
  }
}


/**
 * Implementation of [module]_operation_[class]().
 */
function transformations_xml_operation_TfFileSaveFromXMLElement() {
  return array(
    'category' => t('XML'),
    'label' => t('Save XML node to file'),
    'description' => t('Saves an XML node object to a file in the local file system, making a document out of a plain element if necessary.'),
  );
}

class TfFileSaveFromXMLElement extends TfOperation {
  /**
   * Overriding TfOperation::hasSideEffects() to return TRUE.
   */
  public function hasSideEffects() {
    return TRUE;
  }

  /**
   * Implementation of TfOperation::inputs().
   */
  protected function inputs() {
    return array('xmlNode', 'filepath');
  }

  /**
   * Implementation of TfOperation::inputInfo().
   */
  protected function inputInfo($inputKey, $propertyKey) {
    if ($inputKey == 'xmlNode') {
      switch ($propertyKey) {
        case 'label':
          return t('XML node');

        case 'description':
          return t('The XML node object that should be written to the file.');

        case 'expectedType':
          return 'php:class:DOMNode';
      }
    }
    elseif ($inputKey == 'filepath') {
      switch ($propertyKey) {
        case 'label':
          return t('File path');

        case 'description':
          return t('The path in the local file system where the XML file will be stored.');

        case 'expectedType':
          return 'php:type:string';
      }
    }
  }

  /**
   * Implementation of TfOperation::outputs().
   */
  protected function outputs() {
    return array('xmlDocument');
  }

  /**
   * Implementation of TfOperation::outputInfo().
   */
  protected function outputInfo($outputKey, $propertyKey) {
    if ($outputKey == 'xmlDocument') {
      switch ($propertyKey) {
        case 'label':
          return t('XML document node');

        case 'description':
          return t('If the given XML node was not a document node, it will be wrapped into one, in order to be able to store the file.');

        case 'expectedType':
          return 'php:class:DOMDocument';
      }
    }
  }

  /**
   * Implementation of TfOperation::execute().
   */
  protected function execute(TfOutput $output) {
    TfDataWrapper::includeWrapper('TfXMLNodeData'); // for TfXMLTools
    $doc = TfXMLTools::asXMLDocumentElement($this->input('xmlNode')->data());
    $path = realpath(trim($this->input('filepath')->data()));

    $originalFormatOutput = $doc->formatOutput;
    $doc->formatOutput = TRUE;
    $success = $doc->save($path);
    $doc->formatOutput = $originalFormatOutput;

    if (!$success) {
      $output->setErrorMessage(t('Unable to save XML node to file "!filename".', array(
        '!file' => $path,
      )));
      return;
    }
    $output->set('xmlDocument', $doc);
  }
}
